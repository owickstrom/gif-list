/* jshint globalstrict: true, esnext: true, browser: true */

'use strict';

import { Observable} from 'rx';

function get(url) {
  return Observable.create(observer => {
    // Do the usual XHR stuff
    var req = new XMLHttpRequest();
    req.open('GET', url);

    req.onload = function() {
      // This is called even on 404 etc
      // so check the status
      if (req.status === 200) {
        // Resolve the promise with the response text
        observer.onNext(req.response);
      }
      else {
        // Otherwise reject with the status text
        // which will hopefully be a meaningful error
        observer.onError(Error(req.statusText));
      }
    };

    // Handle network errors
    req.onerror = function() {
      observer.onError(Error("Network Error"));
    };

    // Make the request
    req.send();
  });
}

class Gifs {
  url(after) {
    return 'http://www.reddit.com/r/gif.json' +
      (after ? '?after=' + after : '');
  }

  entries(after) {
    return get(this.url(after))
      .map(JSON.parse)
      .flatMap(response => {
        var entries = Observable.from(response.data.children.map(child => {
          return child.data;
        }));

        if (response.data.after) {
          // Continue with next page.
          return entries.concat(this.entries(response.data.after));
        } else {
          return entries;
        }
      })
      .filter(entry => /.gif$/.test(entry.url));
  }
}

export default new Gifs();

