'use strict';

import React from 'react';
import { Observable} from 'rx';
import Gifs from './gifs';

function toState(entries) {
  return {
    entries: entries
  };
}

function atRate(rate, o) {
  return Observable.zip(
    o,
    // Emit values at a fixed rate, starting directly.
    Observable.just(0).concat(Observable.interval(rate)),
    (v) => v);
}

class GifList extends React.Component {
  constructor(props) {
    super(props);
    this.state = { entries: [] };
  }
  componentDidMount() {
    this.entries =
      atRate(10000, Gifs.entries().bufferWithCount(3))
      .map(toState)
      .forEach(this.setState.bind(this));
  }
  componentWillUnmount() {
    this.entries.unsubscribe();
  }
  render() {
    var figures = this.state.entries.map(entry => {
      var authorUrl = 'http://www.reddit.com/user/' + entry.author;
      return (
        <figure key={entry.name}>
          <img src={entry.url} />
          <figcaption>
            {"\"" + entry.title + "\" by " }
            <a href={authorUrl} rel="author">
              {entry.author}
            </a>
          </figcaption>
        </figure>
      );
    });

    return (
      <div className="gifs">
        {figures}
      </div>
    );
  }
}

export default GifList;
